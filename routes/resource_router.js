const e = require('express');
const express = require('express')
const router = express.Router();

const resourceController = require('../controllers/resource_controller');

router.get('/', async (req, res) => {
    const resourceResult = await resourceController.getResource();
    if (!resourceResult || Object.entries(resourceResult).length === 0) {
        res.status(404).send('No resource');
    } else {
        res.send(resourceResult);
    }
})

router.post('/', (req, res) => {
    const jsonValue = JSON.stringify(req.body);

    resourceController.setResource({ jsonValue });

    res.send('successfuly set resource');
});

module.exports = router;