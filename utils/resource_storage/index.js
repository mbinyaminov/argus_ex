const resource_storage_path = process.env.NODE_ENV !== 'production'
    ? './resource_storage_memory'
    : './resource_storage_redis';
const resource_storage = require(resource_storage_path);

// This file is responsible to server a different resource_storage based on the node_env
// for dev -> memory resource storage
// for prod -> redis resource storage

module.exports = {
    ...resource_storage,
}