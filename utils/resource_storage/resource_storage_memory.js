let my_resource_value = '';

async function getResource() {
    return my_resource_value;
}

async function setResource({ jsonValue }) {
    my_resource_value = jsonValue;
}


module.exports = {
    getResource,
    setResource,
};