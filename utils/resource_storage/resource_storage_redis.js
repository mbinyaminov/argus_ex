const redis = require('async-redis');

const host = process.env.redis_host;
const port = process.env.redis_port;
const password = process.env.redis_password;

if (!host || !port || !password) {
    throw new Error("can't connect to redis -> bad connection values");
}

const redisClient = redis.createClient({ host, port, password });

const CONSTANT_VALUE_KEY = 'argus_security';

redisClient.on("error", function (error) {
    console.error(error);
});

async function getResource() {
    return await redisClient.get(CONSTANT_VALUE_KEY);
}

async function setResource({ jsonValue }) {
    redisClient.set(CONSTANT_VALUE_KEY, jsonValue);
}


module.exports = {
    getResource,
    setResource,
};