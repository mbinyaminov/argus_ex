const resourceController = require('../../controllers/resource_controller');

const assert = require('assert');

describe('Resource Controller', function () {
    it('should export getResource function', function () {
        assert(resourceController.getResource);
    });
    it('should export setResource function', function () {
        assert(resourceController.setResource);
    });
    it('should set a value successfuly', function (done) {
        const value = 'my_value';
        resourceController.setResource({ jsonValue: value })
            .then(() => {
                return resourceController.getResource()
            }).then((result) => {
                assert(result === value);
                return resourceController.setResource({ jsonValue: '' });
            }).then(() => {
                done();
            })
    });
    it('should return an empty value by default', function () {
        return resourceController.getResource()
            .then((resourceValue) => {
                assert(resourceValue === '');
            });
    });
});