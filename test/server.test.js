const server = require('../server');

const assert = require('assert');

describe('Server', function () {
    const routes = ['/resource', 'api-docs'];

    const serverRouter = server._router;

    it('routes defined', function () {
        routes.forEach((route) => {
            const match = serverRouter.stack.find(
                (s) => s.regexp.toString().includes(route)
            );
            assert(match);
        });
    });
});