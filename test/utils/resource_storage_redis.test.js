const redis = require('async-redis');
const sinon = require('sinon');

const getSpy = sinon.spy();
const setSpy = sinon.spy();

sinon.stub(redis, 'createClient').returns({
    on: () => { },
    get: getSpy,
    set: setSpy,
});

const resourceStorageRedis = require('../../utils/resource_storage/resource_storage_redis');

const assert = require('assert');

describe('Server', function () {
    it('set object calles redis set', function () {
        const value = 'some-value';
        resourceStorageRedis.setResource({ jsonValue: 'value' })
        assert(setSpy.calledOnce);
    });
    it('get object calles redis get', function () {
        resourceStorageRedis.getResource()
        assert(getSpy.calledOnce);
    });
});