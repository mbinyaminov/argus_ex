const resourceController = require('../../controllers/resource_controller');

const resourceRouter = require('../../routes/resource_router');

const assert = require('assert');

describe('Resource Router', function () {
    const routes = [
        { path: '/', method: 'get' },
        { path: '/', method: 'post' },
    ]

    it('routes defined', function () {
        routes.forEach((route) => {
            const match = resourceRouter.stack.find(
                (s) => s.route.path === route.path && s.route.methods[route.method]
            );
            assert(match);
        });
    });
});