const resourceStorage = require('../utils/resource_storage');

async function getResource() {
    return await resourceStorage.getResource();
}

async function setResource({ jsonValue }) {
    await resourceStorage.setResource({ jsonValue });
}

module.exports = {
    getResource,
    setResource,
};