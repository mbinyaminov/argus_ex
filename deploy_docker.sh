. ~/set_env_var.sh
docker build --build-arg redis_host_arg=${redis_host} --build-arg redis_port_arg=${redis_port} --build-arg redis_pass_arg=${redis_password} -t argus_ex .
docker run -it -p 80:7861 argus_ex