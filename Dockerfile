from node:14-slim
WORKDIR /app
COPY package.json /app
RUN npm install
ARG redis_host_arg="default value"
ARG redis_port_arg="default value"
ARG redis_pass_arg="default value"
ENV redis_host=${redis_host_arg}
ENV redis_port=${redis_port_arg}
ENV redis_password=${redis_pass_arg}
COPY . /app
CMD ["npm", "start"]