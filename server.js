const express = require('express');
const app = express();
const port = 7861;

app.use(express.json());

const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');

const resourceRouter = require('./routes/resource_router');

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
app.use('/resource', resourceRouter);

app.listen(port, () => {
  console.log(`Server running...`);
});

module.exports = app;